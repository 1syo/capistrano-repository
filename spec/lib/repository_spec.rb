require 'spec_helper'

describe Capistrano::Repository do
  include_context "capistrano"

  describe "#repository" do
    let(:url) { "git://github.com/1syo/capistrano-repository.git" }

    before do
      Capistrano::Repository.load_into(subject)
      Object.any_instance.stub(:`).with("git config remote.origin.url") { url }
    end

    its(:repository) { should eq url }
  end
end
