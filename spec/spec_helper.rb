$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))
$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'bundler/setup'
Bundler.require

require 'rspec'
require 'capistrano-spec'
require 'capistrano_shared_context'
require 'pry'

RSpec.configure do |config|
  config.mock_with :rspec
  config.order = "random"
end
