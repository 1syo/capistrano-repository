require "capistrano"
require "capistrano/repository/version"

module Capistrano::Repository
  def self.load_into(configuration)
    configuration.load do
      def repository
        `git config remote.origin.url`.chomp
      end
    end
  end
end

if Capistrano::Configuration.instance
  Capistrano::Repository.load_into(Capistrano::Configuration.instance)
end
