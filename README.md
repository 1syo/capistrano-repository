# Capistrano::Repository

[![Build Status](https://travis-ci.org/1syo/capistrano-repository.png?branch=master)](https://travis-ci.org/1syo/capistrano-repository)

Automatically select git remote repository in capistrano recipes.

## Installation

Add this line to your application's Gemfile:

    gem 'capistrano-repository', require: false, github: '1syo/capistrano-repository'

And then execute:

    $ bundle

## Usage

Your config/deploy.rb

    require 'capistrano/repository'

    set :scm, :git
    set :repository, repository

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
